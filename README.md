#The corpident package

The purpose of the corpident package is to demonstrate how to create an R package with style files.

Use `devtools::install_bitbucket("thierry_onkelinx/corpident")` to install the package from within R. This requires the 'devtools' package to be installed. Use `install.packages("devtools")` in case it is not available on your system.

Note that additional steps are required to get all examples in the package working. These steps are described in the package [vignette](vignettes/corpident.Rmd).
